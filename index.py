age = 47

name = "Kam"

print("Kam is freaking awesome")
print(age)
print(name)
# need an f string to use the brackets with variables
print(f"Hello my name is {name} and I'm {age} years old")

if age > 18:
    print("You is old")
else:
    print("You're a whippersnapper")


todayIsCold = False

if todayIsCold:
    print("Brrrrrrrrr")
else:
    print("it's balmy yo")


temp = 95

if temp > 90:
    print("Feeling hot hot hot")
    print("Stay hydrated!!")
    print("Put shoes on your pet if you're taking them outside!")
else:
    print("This is much better")
    print("It's not too hot")

# this is a comment

if name == "Kam":
    print("What a great name")
else:
    print("who dis")

year = 1830

if year >= 2000 < 2099:
    print('Welcome to the 21st century!')
else:
    print('You are before or after the 21st century')


# def is short for define to create a function

# name and age are parameters
def hello(name='booboo', age=67):
    print("Hi {} you are {} years old".format(name, age))

hello()


def hello2(name, age):
    return "Hi {} you are {} years old".format(name, age)

sentence = hello2(name, age)

print(hello2(name, age))


def tripleprint(greeting='hello'):
    return greeting + greeting + greeting

print(tripleprint())


word = 'hello'
def tripleprint2(word):
    return word*3

print(tripleprint2(word))


# lists

dognames = ['fido', 'baxter', 'muffin', 'munch']
print(dognames)
# add to list
dognames.insert(1, 'jessie')
print(dognames)
print(dognames[2])

